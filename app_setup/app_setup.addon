# Name: app_setup
# Description: Installs cluster locally on Minishift
# Required-Vars: PRIVATE_KEY_LOCATION
# OpenShift-Version: >=3.7.1

# Create develop project where all apps will live
oc new-project develop

# Create a secret from your private key used for the SCM
oc create secret generic scmsecret --from-file=ssh-privatekey=#{PRIVATE_KEY_LOCATION}
oc secrets link builder scmsecret

# Create a ci-cd project where all cd-cd apps will live ie Jenkins, Sonarqube
oc new-project ci-cd

# Create a secret from your private key used for the SCM
# ci-cd needs these for the build-configurations
oc create secret generic scmsecret --from-file=ssh-privatekey=#{PRIVATE_KEY_LOCATION}
oc secrets link builder scmsecret

# Spin up a Jenkins app
oc new-app --template=jenkins-persistent

# Give Jenkins permission to start builds / deployments in the develop project
oc adm policy add-role-to-user edit system:serviceaccount:ci-cd:jenkins -n develop

# Make sure Jenkins is allowed to create projects and apps for arquillian tests
oc adm policy add-cluster-role-to-user self-provisioner system:serviceaccount:ci-cd:jenkins

# Create all build configuration for each app in the ci-cd project

oc create -f ci-cd/apps/openshift-java-app/buildconfig.yaml

oc project develop

#create all configmaps
oc create -f develop/apps/openshift-java-app/config.yaml

# Create all apps in the develop namespace
oc create -f develop/apps/openshift-java-app/app.yaml
