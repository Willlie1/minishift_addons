#Readme
This readme details development environment in minishift.

##Prerequisites
This plugin requires ssh key **without** passphrase. To generate such a key execute the following.
```bash
ssh-keygen -t rsa -b 4096 -C "<your_email@example.com>"
```
>When you're prompted to "Enter a file in which to save the key," press Enter. This accepts the default file location.

>At the prompt, type a secure passphrase just hit enter. This will skip the passphrase

You have to also add the key to the gitlab. [Click here for instructions](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html#SetupanSSHkey-Step5.AddthepublickeytoyourBitbucketsettings)

To add the rsa key to your clipboard you can do

```bash
sudo apt-get install xclip
# Downloads and installs xclip. When already installed you can skip this one.
xclip -sel clip < <path_to_your_public_key (i.e. ~/.ssh/id_openshift_rsa.pub)>
# Copies the contents of the id_openshift_rsa.pub file to your clipboard
```

##Installation instructions
Execute the installation command as described in the README of the root of this repo.

This plugin needs your private key to create secrets for the SCM. You need to apply this plugin with the _required_ param PRIVATE_KEY_LOCATION. 
```bash
minishift addon apply app_setup --addon-env PRIVATE_KEY_LOCATION=<path_to_private_key>
``` 
The plugin will now create 2 projects in your minishift

* ci-cd
* develop

You can remove all applications with all associated resources.
```bash
minishift addon remove app_setup
```

##CI-CD project
This project contains your Jenkins instance. Also it has the following build-configs:

* openshift-java-app

##Develop project
This project contains app applications. For now this project contains the following:

* openshift-java-app