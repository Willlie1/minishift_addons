#Readme

addons to help setup minikube easily

##How to install an addon
Install an addon by executing the following command:
```bash
minishift addon install <ADDON_NAME>
``` 

Execute the following command to uninstall the addon:
```bash
minishift addon uninstall <ADDON_NAME>
```

Note: An addon should be installed before you can apply it!

Instructions of applying addons are described in the addon specific README.md files.

##Current addons:
- app_setup